import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { AboutComponent } from './components/about/about.component';
import { ContactComponent } from './components/contact/contact.component';
import { FooterComponent } from './components/footer/footer.component';
import { PriceComponent } from './components/price/price.component';
import { HeaderComponent } from './components/header/header.component';
import { SlideShowComponent } from './components/slide-show/slide-show.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';  
import { OwlModule } from 'ngx-owl-carousel';
import { LogoHeaderComponent } from './components/logo-header/logo-header.component';
import { MainNavComponent } from './components/main-nav/main-nav.component';
import { CallBackComponent } from './components/call-back/call-back.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';  
import { MatCarouselModule } from '@ngmodule/material-carousel';

import { OurServicesComponent } from './components/our-services/our-services.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ContactComponent,
    FooterComponent,
    PriceComponent,
    HeaderComponent,
    SlideShowComponent,
    LogoHeaderComponent,
    MainNavComponent,
    CallBackComponent,
    OurServicesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    OwlModule,
    BrowserAnimationsModule,
    MatCarouselModule.forRoot()
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
