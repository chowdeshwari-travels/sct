import { Component, OnInit } from '@angular/core';
import { MatCarousel, MatCarouselComponent } from '@ngmodule/material-carousel';

@Component({
  selector: 'app-slide-show',
  templateUrl: './slide-show.component.html',
  styleUrls: ['./slide-show.component.css']
})
export class SlideShowComponent implements OnInit {
  slides = [
    {id: 1, img: 'assets/img/TT/1.jpg'},
    {id: 2, img: 'assets/img/TT/2.jpg' },
    {id: 3, img: 'assets/img/TT/3.jpg' },
    {id: 4, img: 'assets/img/TT/4.jpg' },
    {id: 5, img: 'assets/img/TT/5.jpg' },
    {id: 6, img: 'assets/img/TT/6.jpg' },
    {id: 7, img: 'assets/img/TT/7.jpg' }
  ];
  constructor() { }
  ngOnInit(): void {

  }

}
